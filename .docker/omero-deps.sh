#!/bin/bash
set -e
set -u
set -x
git clone -b master      https://gitlab.com/openmicroscopy/incubator/omero-dsl.git /tmp/omero-dsl
git clone -b hibernate_3 https://gitlab.com/openmicroscopy/incubator/omero-all.git /tmp/omero-all
cd /tmp/omero-dsl
gradle publishToMavenLocal
cd /tmp/omero-all
git submodule update --init
gradle publishToMavenLocal
