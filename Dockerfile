# Development Dockerfile for OMERO
# --------------------------------
# This dockerfile can be used to build a
# distribution which can then be run
# within a number of different Docker images.

# By default, building this dockerfile will use
# the IMAGE argument below for the runtime image.
ARG IMAGE=openjdk:8-jre-alpine

# To install the built distribution into other runtimes
# pass a build argument, e.g.:
#
#   docker build --build-arg IMAGE=openjdk:9 ...
#

# Similarly, the BUILD_IMAGE argument can be overwritten
# but this is generally not needed.
ARG BUILD_IMAGE=gradle:jdk8

#
# Build phase: Use the gradle image for building.
#
FROM ${BUILD_IMAGE} as build
USER root
RUN cat /etc/apt/sources.list
RUN apt-get update && apt-get install -y zeroc-ice-all-dev
USER 1000
COPY .docker/omero-deps.sh /tmp/omero-dep.sh
RUN bash /tmp/omero-dep.sh
USER root
COPY . /src
RUN chown -R 1000 /src
USER 1000
WORKDIR /src
RUN gradle assemble
RUN mkdir -p build/unpack
WORKDIR /src/build/unpack
RUN tar --strip-components=1 -xvf ../distributions/*.tar

#
# Install phase: Copy the build fat jar into a
# clean container to minimize size.
#
FROM ${IMAGE}
COPY --from=build /src/build/unpack/ /opt/omero-ms-mapr/
ENTRYPOINT ["/opt/omero-ms-mapr/bin/omero-ms-mapr"]
