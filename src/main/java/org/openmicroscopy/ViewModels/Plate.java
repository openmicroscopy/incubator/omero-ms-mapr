package org.openmicroscopy.ViewModels;

import java.util.Map;

public class Plate {
    public long id;
    public long ownerId;
    public long childCount;
    public String name;
    public String permsCss;
    public Map<String, String> extra;
}
