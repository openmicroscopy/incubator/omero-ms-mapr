package org.openmicroscopy.controllers;

import org.openmicroscopy.verticles.LoginVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginController {

    public static String API_LOGIN = "/login";

    private static final Logger log =
            LoggerFactory.getLogger(LoginController.class);

    private Vertx vertx;
    private Router router;

    public LoginController(Vertx vertx) {
        this.vertx = vertx;
    }

    public Router getRouter() {
        if (router == null) {
            router = Router.router(vertx);
            router.post("/").handler(this::postLogin);
        }
        return router;
    }

    public void postLogin(RoutingContext rc) {
        vertx.eventBus().<String>send(LoginVerticle.POST_LOGIN, rc.getBodyAsString(), result -> {
            HttpServerResponse response = rc.response();
            try {
                if (result.failed()) {
                    // setError(response, result.cause());
                    return;
                }
                Message<String> res = result.result();
                JsonObject data = new JsonObject(res.body());

                // Store session id in session store
                rc.session().put("sessionId", data.getString("sessionId"));
                rc.response().headers().setAll(res.headers());
                rc.response().write(data.encode());
            } finally {
                response.end();
                log.debug("Response ended");
            }
        });
    }

}
