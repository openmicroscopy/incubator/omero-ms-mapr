package org.openmicroscopy.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import ome.api.ISession;
import ome.model.meta.Session;
import ome.security.SecuritySystem;
import ome.services.sessions.SessionBean;
import ome.services.util.Executor;
import ome.system.Principal;
import ome.system.Roles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Collections;
import java.util.Map;

public class LoginVerticle extends AbstractVerticle {

    private static final Logger Log =
            LoggerFactory.getLogger(LoginVerticle.class);

    public static final String POST_LOGIN =
            "omero.post_logon";

    ApplicationContext appContext;
    ISession sessionBean;
    SecuritySystem securitySystem;
    Executor executor;

    public LoginVerticle(ApplicationContext omeroContext) {
        appContext = omeroContext;
        executor = (Executor) appContext
                .getBean("executor");
        securitySystem = (SecuritySystem) appContext
                .getBean("securitySystem");
        sessionBean = (SessionBean) appContext
                .getBean("internal-ome.api.ISession");
    }

    @Override
    public void start() throws Exception {
        super.start();

        vertx.eventBus().consumer(POST_LOGIN, this::postLogin);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public void postLogin(Message<String> msg) {
        JsonObject body = new JsonObject(msg.body());
        String username = body.getString("username");
        String password = body.getString("password");

        try {
            Roles roles = securitySystem.getSecurityRoles();
            Principal p = new Principal(username, roles.getSystemGroupName(), "Test");
            Session s = sessionBean.createSession(p, password);

            Map<String, String> result =
                    Collections.singletonMap("sessionId", s.getUuid());
            String json = Json.encode(result);

            MultiMap headers = MultiMap.caseInsensitiveMultiMap()
                    .set("Content-Type", "application/json")
                    .set("Content-Length", String.valueOf(json.length()));

            msg.reply(json, new DeliveryOptions().setHeaders(headers));
        } catch (Exception e) {
            msg.fail(404, "Cannot acquire session:");
        }
    }

}
